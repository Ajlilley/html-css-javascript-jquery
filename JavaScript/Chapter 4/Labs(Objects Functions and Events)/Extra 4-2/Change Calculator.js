var calculate = function () {
    var changeDue = document.getElementById("changeDue").value;
    var newTotal = changeDue;

    if (changeDue > 0) {
        var quarters = Math.floor(changeDue / 25);
        changeDue = changeDue % 25;
        document.getElementById("quarters").value = quarters;

        var dimes = Math.floor(changeDue / 10);
        changeDue = changeDue % 10;
        document.getElementById("dimes").value = dimes;

        var nickels = Math.floor(changeDue / 5);
        document.getElementById("nickels").value = nickels;

        var pennies = changeDue % 5;
        document.getElementById("pennies").value = pennies;
    };
}

window.onload = function () {
    document.getElementById("calculate").onclick = calculate;
};
