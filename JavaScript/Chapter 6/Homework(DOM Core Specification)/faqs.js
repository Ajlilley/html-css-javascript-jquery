"use strict";
var $ = function (id) {
    return document.getElementById(id);
};
var toggle = function () {
    var link = this;
    var h2 = link.parentNode;
    var div = h2.nextElementSibling;

    if (h2.hasAttribute("class")) {
        h2.removeAttribute("class");
    } else {
        h2.className = "minus";
    }

    if (div.hasAttribute("class")) {
        div.removeAttribute("class");
    } else {
        div.className = "open";
    }
};

window.onload = function () {
    var faqs = $("faqs");
    var linkToElements = faqs.getElementsByTagName("a");

    for (var i = 0; i < linkToElements.length; i++) {
        linkToElements[i].onclick = toggle;
    }
    linkToElements[0].focus();
};
