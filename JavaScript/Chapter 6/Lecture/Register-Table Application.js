"use strict";
var $ =
    function (id) {
        return document.getElementById(id);
    };

var processEntries = function () {
        var header = "";
        var html = "";
        var required = "<span>Required field</span>";
        var msg = "Please review your entries and " +
            "complete all required fields";

        var email = $("email_address").value;
        var phone = $("phone").value;
        var country = $("country").value;
        var contact = "Text";
        if ($("email").checked) {
            contact = "Email";
        }
        if ($("none").checked) {
            contact = "None";
        }
        var terms = $("terms").checked;
        if (email == "") {
            email = required;
            header = msg;
        }
        if (phone == "") {
            phone = required;
            header = msg;
        }
        if (country == "") {
            country = required;
            header = msg;
        }
        if (terms == false) {
            terms = required;
            header = msg;
        }
