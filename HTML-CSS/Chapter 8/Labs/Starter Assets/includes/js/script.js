/*jslint browser: true*/
/*global $, jQuery*/

$(function () {
    'use strict';
    $('#alertMe').click(function (e) {
        e.preventDefault();

        $('#successAlert').slideDown();
    });
});

$('a.pop').click(function (e) {
    'use strict';
    e.preventDefault();

    $('a.pop').popover();

    $('[rel="tooltip"]').tooltip();
});
